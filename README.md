# My dot files 

I will show two code editors convenient for real-time syncing with remote clusters. Also, tmux setups will be covered.

## Openvpn and VPN configuration
- https://openvpn.net/community-downloads/
- ``Informatics-InfNets-AT.ovpn``(faster) or ``Informatics-EdLAN-AT.ovpn`` from https://groups.inf.ed.ac.uk/inf-unit/OpenVPN/Config/

## Sublime text 3
Pros and cons:
- Save code both locally and remotely
- Might accidently download large files (models) and break

Requirement:
- Downlaod from https://www.sublimetext.com/
- Install package control: https://packagecontrol.io/installation
- Install sftp for sublime: https://codexns.io/products/sftp_for_sublime/install

## VScode sftp
Pros and cons: same as sublime

Requirement:
- Install: https://marketplace.visualstudio.com/items?itemName=Natizyskunk.sftp
- In VS Code, open a local directory you wish to sync to the remote server (or create an empty directory that you wish to first download the contents of a remote server folder in order to edit locally).
- Ctrl+Shift+P on Windows/Linux or Cmd+Shift+P on Mac open command palette, run SFTP: config command.
- A basic configuration file will appear named sftp.json under the .vscode directory, open and edit the configuration parameters with your remote server information.
- Modify .vscode/sftp.json file

```
{
    "name": "mlp",
    "host": "mlp.inf.ed.ac.uk",
    "protocol": "sftp",
    "port": 22,
    "username": "your_school_id",
    "remotePath": "/home/your_school_id/folder",
    "uploadOnSave": true,
    "downloadOnOpen": true,
    "useTempFile": false,
    "openSsh": false,
    "interactiveAuth": true,
    "ignore":[
        ".vscode", ".git", ".DS_Store",
        "\\.sublime-(project|workspace)", "sftp-config(-alt\\d?)?\\.json",
        "sftp-settings\\.json", "/venv/", "\\.svn/", "\\.hg/", "\\.git/",
        "\\.bzr", "_darcs", "CVS", "\\.DS_Store", "Thumbs\\.db", "desktop\\.ini",
        "__pycache__/", "ckpt/", "debug/", "figures/", "exp/"
    ]
}
```
- Save and close the sftp.json file.
- Ctrl+Shift+P on Windows/Linux or Cmd+Shift+P on Mac open command palette.
- Type sftp and you'll now see a number of other commands. You can also access many of the commands from the project's file explorer context menus.
- A good one to start with if you want to sync with a remote folder is SFTP: Download Project. This will download the directory shown in the remotePath setting in sftp.json to your local open directory.
- Done - you can now edit locally and after each save it will upload to sync your remote file with the local copy.
Enjoy!



## VScode ssh (Not recommended)
Pros and cons:
- No need to choose what file to download to local machine
- Might be killed by the administrator because there will be background programs constantly running on the remote machine to sync the file

Requirement:
- Download from https://code.visualstudio.com/download
- Install ssh for vscode https://code.visualstudio.com/docs/remote/ssh-tutorial

## Tmux and cluster shortcuts
- Clone the three following files to your home directory on the clusters
    - `.bashrc`, `.bash_profile`, `.tmux.conf`
- ``git clone https://github.com/cdt-data-science/cluster-scripts.git``
- `source ~/.bashrc`
- Cluster shortcuts: ``free-gpus``, `myjobs`, `whoson`
- Tmux: `tmux`
- Tmux cheat sheet: https://tmuxcheatsheet.com/. Please be aware that my configuration use 'Ctrl + a' as prefix not 'Ctrl + b'. 
