export PATH=/home/$USER/cluster-scripts:$PATH
. /home/htang2/toolchain/toolchain.rc
alias tmua='tmux a -t'
alias {sl,ks,sk,l}='ls'
alias ll='ls -l'

export LS_OPTIONS='--color=auto'
eval "$(dircolors -b)"
alias ls='ls $LS_OPTIONS'
